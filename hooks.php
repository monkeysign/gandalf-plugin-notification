<?php
/**
 * Aggiungo il path delle views di questo plugin grazie al filtro "views_path" che si trova nel controller generale
 */
//ordini@blobvideo.com
const EMAIL_ADMIN = 'info@blobvideo.com';
const EMAIL_REGISTRAZIONE = 'registrazioni@blobvideo.com';
const EMAIL_ADMIN_ORDINI = 'ordini@blobvideo.com';


const CRM_CUSTOMER_SEND_MAIL = 'CRM_CUSTOMER_SEND_MAIL';
const CRM_BUY_SEND_MAIL = 'CRM_BUY_SEND_MAIL';
const CRM_CONTACT_SEND_MAIL = 'CRM_CONTACT_SEND_MAIL';
const CRM_ORDER_STATUS_SEND_MAIL = 'CRM_ORDER_STATUS_SEND_MAIL';
const CRM_RECUPERA_PASSWORD_SEND_MAIL = 'CRM_RECUPERA_PASSWORD_SEND_MAIL';
const CRM_ADMIN_REGISTER_SEND_MAIL = 'CRM_ADMIN_REGISTER_SEND_MAIL';


/**
 * Aggiungo il path delle views di questo plugin grazie al filtro "views_path" che si trova nel controller generale
 */
hooks()->add_filter(APP_VIEWS_PATH, function ($views = []) {
    return array_merge([__DIR__ . '/views/'], $views);
});


// Come richiamarlo  hooks()->do_action( CRM_CUSTOMER_SEND_MAIL, $item );

hooks()->add_action(CRM_ORDER_STATUS_SEND_MAIL, function ($order) {

    $param['email-from'] = EMAIL_ADMIN_ORDINI;
    $param['email'] = $order->billing_address()->email;

    $linkSito = path_for('frontpage');
    $date = date_create($order->date);
    $dataOrdine = date_format($date, 'd/m/Y');
    $tabella = '<table rules="all" style="border-color: #666;text-align:center" border="2" cellpadding="10">'
        . '<tr><th><strong>Prodotto</strong> </th><th>Codice</th><th>Stato</th><th>Quantit&agrave;</th><th>Prezzo</th></tr>';

    $prezzoTotaleRighe = 0;
    if ($order->orderProduct) {
        foreach ($order->orderProduct as $prodotto) {

            if ($prodotto->product->state_product == 1)
                $stato = "Usato";
            else {
                $stato = "Nuovo";
            }

            $tabella .= '<tr><td>' . $prodotto->title . '</td><a href="' . path_for('product', ['permalink' => $prodotto->product->getPermalink()]) . '">' . $prodotto->product->permalink . '</a></td><td>' . $stato . '</td><td>' . $prodotto->quantity . '</td><td>' . $prodotto->price . ' &euro;</td></tr>';

            $prezzoTotaleRighe += $prodotto->price * $prodotto->quantity;
        }
    }

    /*recupero le spese di spedizione*/
    $spedizione = \Plugins\ECOMMERCE\Models\Shipment::where('id', $order->id_ship)->first();

    if ($order->cost_ship == 0) {
        $spedizione->label_cart = "Gratis";
    } else {
        $spedizione->label_cart = $order->cost_ship . " €";
    }

    $costoGestione = 0;
    if ($order->payment_type == 2) {
        $costoGestione = $order->total - $prezzoTotaleRighe - $order->cost_ship;
    }

    $tabella .= '<tr><td colspan="2">Metodo di Spedizione selezionato</td>
<td colspan="1">' . $spedizione->title . '</td>
<td colspan="1">Prezzo</td>
<td colspan="1">' . $spedizione->label_cart . '</td></tr>';
    //Metodo di pagamento
    $tabella .= '<tr><td colspan="2">Metodo di Pagamento selezionato</td>
<td colspan="1">' . $order->typePayment->title . '</td>
<td colspan="1">Costo Gestione</td>
<td colspan="1">' . $costoGestione . '</td></tr>';
    $tabella .= '<tr><td colspan="4" style="text-align:right">Totale :</td><td>' . $order->total . ' &euro;</td></tr>';
    $tabella .= '</table>';


    if ($order->status == 2) { //spedito
        $oggetto = "Spedizione Ordine N. " . $order->id;
        $TestoCambioStato = "<p>Gentile Cliente,<br/>grazie per aver effettuato acquisti sul nostro sito internet.<br/>Il suo ordine &egrave; stato spedito oggi e le sar&agrave; consegnato nei prossimi giorni. <br/><br/>Codice Tracking:" . $order->tracking . " <br /><br />Puoi visualizzare lo stato della spedizione cliccando <a href='https://www.gls-italy.com/index.php?option=com_gls&lang=it&mode=page_search&view=track_e_trace'>qui</a></p>Riepilogo ordine N." . $order->id . " del " . $dataOrdine . "<br/><br/>" . $tabella;
    } else if ($order->status == 4) {//Annullato
        $oggetto = " Annullamento Ordine N. " . $order->id;
        $TestoCambioStato = "<p>Gentile Cliente,<br/>il suo ordine &egrave; stato annullato.<br/><br/>Riepilogo ordine N." . $order->id . " del " . $dataOrdine . "<br/><br/>" . $tabella . "<p>Se vuole, pu&ograve; effettuare un nuovo ordine sul nostro sito internet <a href='" . $linkSito . "'>www.blobvideo.com</a></p>";
    } else if ($order->status == 5) {//Rimborsato
        $oggetto = "Rimborso  Ordine N. " . $order->id;
        $TestoCambioStato = "<p>Gentile Cliente,<br/>il suo ordine &egrave; stato rimborsato.<br/><br/>Riepilogo ordine N." . $order->id . " del " . $dataOrdine . "</p><br/><br/>" . $tabella . "<p>Se vuole, pu&ograve; effettuare un nuovo ordine sul nostro sito internet <a href='" . $linkSito . "'>www.blobvideo.com</a></p>";
    } else if ($order->status == 6) {
        $oggetto = " Ritiro Ordine N. " . $order->id;
        $TestoCambioStato = "<p>Gentile Cliente,<br/>grazie per aver effettuato acquisti sul nostro sito internet.<br/>Il suo ordine &egrave; stato ritirato in sede.<br/><br/>Riepilogo ordine N." . $order->id . " del " . $dataOrdine . "<br/><br/>" . $tabella . "<p>";
    }

    $messaggio = "<div style='padding:10px;color:black;'>
                <hr>
                <div>
                " . $TestoCambioStato . "
                </div>
               <div style='float:left;'>
                <p style='font-size:13px;margin-top:15px'>
                Cordiali Saluti,<br/>Staff Blob Video <br/><a href='" . $linkSito . "'>www.blobvideo.com</a></p></div>
                </div>";

    //$param['msg'] = "Gentile Cliente, le confermiamo che la sua registrazione a www.blobvideo.com è avvenuta con successo.";
    $param['subject'] = "Blob Video : " . $oggetto;
    $param['msg'] = $messaggio;

    notification_send_mail($param);
});

hooks()->add_action(CRM_CUSTOMER_SEND_MAIL, function ($customer) {
    $param['email-from'] = EMAIL_ADMIN;
    $param['email'] = $customer['email'];
    $param['subject'] = "Blob Video : Registrazione Completa!";

    $messaggio = "<div style='padding:10px;color:black;'>
               
                <hr>
                <table>
                <tr><td>
                <div>
                                       
                    <p> Grazie per esserti registrato al nostro sito internet. </p>
                    <p> Adesso puoi:<br/> 
Effettuare acquisti <br/>
Tenere traccia dei tuoi ordini<br/> 
Creare e gestire la tua Lista dei desideri<br/> 
Ricevere avvisi quando un prodotto torna disponibile<br/> 
Rivolgerti al nostro Servizio di Assistenza Tecnica in negozio<br/> 
Valutare il tuo Usato online <br/>
E tanto altro ancora! <br/>
Effettua subito il Login su <a href='https://www.blobvideo.com/customer/access'>www.blobvideo.com</a> 
                    </p>
                </div>
                </td>
                </tr>
                <tr>
                <td><div style='float:left;'>
                <p style='font-size:13px;margin-top:15px'>
                Per qualsiasi informazione non esitare a contattarci.<br/>Cordiali Saluti,<br/>Staff Blob Video</p></div></td>
                </tr>
                </table>
                </div>";

    //$param['msg'] = "Gentile Cliente, le confermiamo che la sua registrazione a www.blobvideo.com è avvenuta con successo.";
    $param['msg'] = $messaggio;

    notification_send_mail($param);
});

// Come richiamarlo  hooks()->do_action( CRM_BUY_SEND_MAIL, $item );

hooks()->add_action(CRM_BUY_SEND_MAIL, function ($order) {
    $param['email-from'] = EMAIL_ADMIN_ORDINI;
    $param['email'] = $order->billing_address()->email;
    $param['subject'] = "Blob Video: Conferma Ordine N. " . $order->id;

    $date = date_create($order->date);
    $dataOrdine = date_format($date, 'd/m/Y');
    $tabella = '<table rules="all" style="border-color: #666;text-align:center" border="2" cellpadding="10">'
        . '<tr><th><strong>Prodotto</strong> </th><th>Codice</th><th>Stato</th><th>Quantit&agrave;</th><th>Prezzo</th></tr>';

    $prezzoTotaleRighe = 0;
    if ($order->orderProduct) {
        foreach ($order->orderProduct as $prodotto) {

            if ($prodotto->product->state_product == 1)
                $stato = "Usato";
            else {
                $stato = "Nuovo";
            }

            $tabella .= '<tr><td>' . $prodotto->title . '</td><a href="' . path_for('product', ['permalink' => $prodotto->product->getPermalink()]) . '">' . $prodotto->product->permalink . '</a></td><td>' . $stato . '</td><td>' . $prodotto->quantity . '</td><td>' . $prodotto->price . ' &euro;</td></tr>';

            $prezzoTotaleRighe += $prodotto->price * $prodotto->quantity;
        }
    }

    /*recupero le spese di spedizione*/
    $spedizione = \Plugins\ECOMMERCE\Models\Shipment::where('id', $order->id_ship)->first();

    if ($order->cost_ship == 0) {
        $spedizione->label_cart = "Gratis";
    } else {
        $spedizione->label_cart = $order->cost_ship . " €";
    }

    $costoGestione = 0;
    if ($order->payment_type == 2) {
        $costoGestione = $order->total - $prezzoTotaleRighe - $order->cost_ship;
    }

    $tabella .= '<tr><td colspan="2">Metodo di Spedizione selezionato</td>
<td colspan="1">' . $spedizione->title . '</td>
<td colspan="1">Prezzo</td>
<td colspan="1">' . $spedizione->label_cart . '</td></tr>';
    //Metodo di pagamento
    $tabella .= '<tr><td colspan="2">Metodo di Pagamento selezionato</td>
<td colspan="1">' . $order->typePayment->title . '</td>
<td colspan="1">Costo Gestione</td>
<td colspan="1">' . $costoGestione . '</td></tr>';
    $tabella .= '<tr><td colspan="4" style="text-align:right">Totale :</td><td>' . $order->total . ' &euro;</td></tr>';
    $tabella .= '</table>';

    //PayPAly
    if ($order->payment_type == 2) {
        $messaggio = "<div style='padding:10px;color:black;'><p>
            Gentile Cliente,<br/>  
            grazie per aver effettuato acquisti sul nostro sito internet. <br/>  
            </p>Riepilogo ordine N. " . $order->id . " del " . $dataOrdine . "<br/><br/>" . $tabella . "
             <p> Pu&ograve; seguire lo stato dell'ordine effettuando il login sul nostro <a href='https://www.blobvideo.com/customer/access'>sito internet</a> 
            <br/>
            Appena l'ordine sar&agrave; affidato al Corriere, ricever&agrave; il codice tracking per poter seguire la spedizione. 
            <br/>
            </p></div>";
    } else {  //Bonifico
        /*$messaggio= "<div style='padding:10px;color:black;'><p>Ciao,<br/>  Hai ricevuto un ordine da: ".$order->billing_address()->name." ". $order->billing_address()->surname ." ( ". $order->billing_address()->email ." )</p>";
        $messaggio.= "<p>Riepilogo Ordine</p>";
        $messaggio.= $tabella;
        $messaggio.= "<p><br/><br/>Accedi alla tua area utente per gestire l'ordine</p></div>";*/

        $messaggio = "<div style='padding:10px;color:black;'><p>  
            Gentile Cliente,<br/>  
            grazie per aver effettuato acquisti sul nostro sito internet. <br/>  
            I prodotti da Lei acquistati saranno tenuti da parte per le prossime 48 ore. <br/>  
            </p>Riepilogo ordine N. " . $order->id . " del " . $dataOrdine . "<br/><br/>" . $tabella . "
            <p>
            Questi sono i dati per il Pagamento con Bonifico:<br/>
            IBAN:<b> IT42 V030 6976 0421 0000 0004 554</b><br/>
            Intestatario:<b> TORTORA CONCETTA </b><br/>
            La causale del Bonifico dovr&agrave; riportare: il numero di riferimento dell'ordine, la data di effettuazione dell'ordine, Nome e Cognome dell'intestatario dell'ordine. <br/>
            Restiamo in attesa della ricevuta di avvenuto Bonifico, da inviarsi via mail all'indirizzo ordini@blobvideo.com <br/>
            </p></div>";
    }

    $messaggio .= "<p style='font-size:13px;margin-top:15px'>Per qualsiasi informazione non esitare a contattarci.<br/>Cordiali Saluti,<br/>Staff Blob Video</p>";
    $param['msg'] = $messaggio;
    notification_send_mail($param);


    /* EMAIL ADMIN*/
    $param['email-from'] = EMAIL_ADMIN_ORDINI;
    $param['email'] = EMAIL_ADMIN_ORDINI;
    $param['subject'] = "Blob Video: Conferma Ordine N. " . $order->id;

    //PayPAly
    if ($order->payment_type == 2) {
        $messaggio = "<div style='padding:10px;color:black;'><p>
            Gentile Cliente,<br/>  
            grazie per aver effettuato acquisti sul nostro sito internet. <br/>  
            </p>Riepilogo ordine N. " . $order->id . " del " . $dataOrdine . "<br/><br/>" . $tabella . "
             <p> Pu&ograve; seguire lo stato dell'ordine effettuando il login sul nostro <a href='https://www.blobvideo.com/customer/access'>sito internet</a> 
            <br/>
            Appena l'ordine sar&agrave; affidato al Corriere, ricever&agrave; il codice tracking per poter seguire la spedizione. 
            <br/>
            </p></div>";
    } else {  //Bonifico
        /*$messaggio= "<div style='padding:10px;color:black;'><p>Ciao,<br/>  Hai ricevuto un ordine da: ".$order->billing_address()->name." ". $order->billing_address()->surname ." ( ". $order->billing_address()->email ." )</p>";
        $messaggio.= "<p>Riepilogo Ordine</p>";
        $messaggio.= $tabella;
        $messaggio.= "<p><br/><br/>Accedi alla tua area utente per gestire l'ordine</p></div>";*/

        $messaggio = "<div style='padding:10px;color:black;'><p>  
            Gentile Cliente,<br/>  
            grazie per aver effettuato acquisti sul nostro sito internet. <br/>  
            I prodotti da Lei acquistati saranno tenuti da parte per le prossime 48 ore. <br/>  
            </p>Riepilogo ordine N. " . $order->id . " del " . $dataOrdine . "<br/><br/>" . $tabella . "
            <p>
            Questi sono i dati per il Pagamento con Bonifico:<br/>
            IBAN:<b> IT42 V030 6976 0421 0000 0004 554</b><br/>
            Intestatario:<b> TORTORA CONCETTA </b><br/>
            La causale del Bonifico dovr&agrave; riportare: il numero di riferimento dell'ordine, la data di effettuazione dell'ordine, Nome e Cognome dell'intestatario dell'ordine. <br/>
            Restiamo in attesa della ricevuta di avvenuto Bonifico, da inviarsi via mail all'indirizzo ordini@blobvideo.com <br/>
            </p></div>";
    }

    $messaggio .= "<p style='font-size:13px;margin-top:15px'>Per qualsiasi informazione non esitare a contattarci.<br/>Cordiali Saluti,<br/>Staff Blob Video</p>";
    $param['msg'] = $messaggio;
    notification_send_mail($param);
});


hooks()->add_action(CRM_CONTACT_SEND_MAIL, function ($item) {

    $param['email-from'] = $item['email'];
    $param['email'] = EMAIL_ADMIN;
    $param['subject'] = "Blob Video : Modulo Contatti " . $item['name'];

    $messaggio = "<div style='padding:10px;color:black;'>
                
                <hr>
                <table>
                <tr><td>
                <div>
                    <p style='font-size:15px'>
                    Modulo contatti compilato da: <b>" . $item['name'] . " (" . $item['email'] . ")</b>.
                    </p>
                    <p>Messaggio:</p>
                    <p>
                        " . $item['msg'] . "
                    </p>
                </div>
                </td>
                </tr>
                
                </table>
                </div>";

    //$param['msg'] = "Gentile Cliente, le confermiamo che la sua registrazione a www.blobvideo.com è avvenuta con successo.";
    $param['msg'] = $messaggio;
    notification_send_mail($param);
    /* $param['email'] = 'info@blobvideo.com';
     notification_send_mail($param);*/
});

hooks()->add_action(CRM_RECUPERA_PASSWORD_SEND_MAIL, function ($customer) {

    $param['email-from'] = EMAIL_ADMIN;
    $param['email'] = $customer['email'];
    $param['subject'] = "Blob Video : Procedura Recupero Password";


    $pathConferma = path_for('recovery-password', ['id_customer' => $customer->id, 'password' => $customer->password]);

    $messaggio = "<div style='padding:10px;color:black;'>
                
                <hr>
                <table>
                <tr><td>
                <div>
                   <p style='font-size:15px'>
                Procedura di rigenerazione password.<br/><br/>
                Clicchi sul seguente link per avviare la procedura di cambio password per il suo account <a href='" . $pathConferma . "'><button style='backgound:#002244;'>CAMBIA PASSWORD</button></a> <br/><br/>*Se non hai avviato tale funzionalit&agrave; basta che ignori la suddetta email<br/><br/></p>
                   </div>
                </td>
                </tr>
                <tr>
                <td><div style='float:left;'>
                <p style='font-size:13px;margin-top:15px'>
                Per qualsiasi informazione non esitare a contattarci.<br/>Cordiali Saluti,<br/>Staff Blob Video</p></div></td>
                </tr>
                </table>
                </div>";


    //$param['msg'] = "Gentile Cliente, le confermiamo che la sua registrazione a www.blobvideo.com è avvenuta con successo.";
    $param['msg'] = $messaggio;
    notification_send_mail($param);
});

hooks()->add_action(CRM_ADMIN_REGISTER_SEND_MAIL, function ($customer) {

    $param['email-from'] = EMAIL_REGISTRAZIONE;
    $param['email'] = $customer['email'];
    $param['subject'] = "Blob Video : Grazie per esserti registrato!";

    $messaggio = "<div style='padding:10px;color:black;'>
                <table>
                <tr><td>
                <div>
                    <p>Gentile {$customer->name} {$customer->surname}</p>               
                    <p>Ti ringraziamo per esserti registrato all'Area Clienti Blob Video, l'area che ti permette di: </p>
                    <p>Effettuare acquisti <br/>
Creare e gestire la tua Lista dei desideri<br/> 
Ricevere avvisi sulla nuova disponibilit&agrave; dei prodottti <br/> 
Velocizzare gli acquisti con le tue preferenze di spedizione <br/> 
Tenere traccia dei tuoi ordini <br/>
Ottenere valutazioni del tuo Usato  <br/>
Usufruire dei nostri servizi di Assistenza Tecnica Specializzata in sede   <br/>
E tanto altro ancora! <br/>
                    </p>
                    <p>Per accedere al tuo account, modificare o recuperare la password, variare l'indirizzo e-mail o cancellare la tua adesione al servizio, utilizza questi dati: </p>
                    <p>E-mail: <strong>{$customer->email}</strong></p>
                    <p>Password: <strong>{$customer->password}</strong></p>
                    <p>Puoi cambiare i dati di accesso in qualsiasi momento collegandoti all'Area Clienti Blob Video.</p>
                </div>
                </td>
                </tr>
                <tr>
                <td><div style='float:left;'>
                <p style='font-size:13px;margin-top:15px'>
                <strong>Questa &egrave; una mail automatica. Si prega di non rispondere.</strong><br/>Per informazioni e supporto puoi scrivere a: email@blobvideo.com <br/>Cordiali Saluti,<br/>Staff Blob Video</p></div></td>
                </tr>
                </table>
                </div>";


    //$param['msg'] = "Gentile Cliente, le confermiamo che la sua registrazione a www.blobvideo.com è avvenuta con successo.";
    $param['msg'] = $messaggio;
    notification_send_mail_smtp($param);
});


/**
 * Includo gli helper
 */
require __DIR__ . '/helpers.php';