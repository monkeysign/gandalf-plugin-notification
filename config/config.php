<?php
return [
	"name"   => 'Notification',
	"class"  => \Plugins\Notification\Plugin::class,
	"routes" => __DIR__ . '/../routes/routes.php'
];