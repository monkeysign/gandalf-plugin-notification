<?php
/**/
/**
 * Invio email
 *
 * @param
 *  $param['email-from'] = "Email del Mittente";
 *  $param['email'] = "Email del Destinatario";
 *  $param['subject'] = "Oggetto"
 *  $param['msg'] = "Messaggio in html"
 *
 * @return return['result'] = esito invio
 *         return['error'] = descrizione errore;
 */
function notification_send_mail($param)
{
    /* @var  $mail \PHPMailer\PHPMailer\PHPMailer */
    $mail = container()->get('mail');
    $mail->ClearAllRecipients();
    try {
        $mail->setFrom($param['email-from']);
        $mail->addAddress($param['email']);
        $mail->Subject = $param['subject'];

        $mail->addEmbeddedImage(asset('assets/images/') . 'icons/call-3.png', 'logo_attach', 'call-3.png', 'base64', 'image/png');
        $mail->Body = $param['msg'];
        $mail->WordWrap = 50;
        $mail->isHTML(true);
        $return['result'] = true;
        if (!$mail->send()) {
            $return['result'] = false;
            $return['error'] = "Mailer Error: " . $mail->ErrorInfo;
        }
    } catch (Exception $e) {
        $return['result'] = false;
        $return['error'] = 'Message could not be sent. Mailer Error: ' . $mail->ErrorInfo;
    }
    return $return;
}

/**
 * Invio email con smtp
 * @param $param
 * @return mixed
 */
function notification_send_mail_smtp($param)
{
    /* @var  $mail \PHPMailer\PHPMailer\PHPMailer */
    $mail = container()->get('mail');
    $mail->ClearAllRecipients();

    try {
        $mail->isHTML(true);
        $mail->IsSMTP();

        $mail->Host = 'mail.your-server.de';
        $mail->Port = 587;
        $mail->SMTPAuth = true;
        $mail->Username = 'registrazioni@blobvideo.com';
        $mail->Password = 'Blobvideo2013';
        $mail->SMTPSecure = 'tls';

        $mail->setFrom($param['email-from']);
        $mail->FromName = 'Blob Video Registrazioni';
        $mail->addAddress($param['email']);
        $mail->Subject = $param['subject'];

        $mail->addEmbeddedImage(asset('assets/images/') . 'icons/call-3.png', 'logo_attach', 'call-3.png', 'base64', 'image/png');
        $mail->Body = $param['msg'];
        $mail->WordWrap = 50;

        $return['result'] = true;
        if (!$mail->send()) {
            $return['result'] = false;
            $return['error'] = "Mailer Error: " . $mail->ErrorInfo;
        }
    } catch (Exception $e) {
        $return['result'] = false;
        $return['error'] = 'Message could not be sent. Mailer Error: ' . $mail->ErrorInfo;
    }
    return $return;
}